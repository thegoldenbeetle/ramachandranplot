# Ramachandran Plots

In `data` dir you can find 3 compounds from the PDB base (**1qg9**, **6cpj** and **6jou**).

In `requirements` dir in file `pip.txt` you can find necessary python packages for code running. Also you should have Python (version >= 3.5), pip3 and jupyter notebook (jupyter core version >= 4.6.1, notebook version >= 6.0.1).

Before running scripts you should run from console:
- sudo pip3 install -r requirements/pip.txt
- sudo pip3 install notebook --upgrade

If any errors will appear during the launch of the script, try to upgrade the ipython, ipykernel, jupyter.

In `res` dir you can find received plots.

In `src` dir you can find source code. 

You can use `draw_ramachandran_plot.py` script for drawing plots and saving they (only show plots available only in jupyter). Interactive mode is only available in jupyter.

Examples of launch `draw_ramachandran_plot.py`:
- python draw_ramachandran_plot.py -f data/6jou.pdb -sc -a PRO GLY
- python draw_ramachandran_plot.py -f data/6jou.pdb -sc -a PRO GLY -fe pro_gly
- python draw_ramachandran_plot.py -f data/6jou.pdb

In `ramachandran_plot.ipynb` you can find launch results of code for drawing Ramachandran plots.

## Received plots:

### Plots

#### 6jou
![6jou](res/6jou_plot.png "6jou")

#### 6cpj
![6cpj](res/6cpj_plot.png "6cpj")

#### 1qg9
![1qg9](res/1qg9_plot.png "1qg9")

### Interactive gifs (what interactive plots look like)

#### 6jou
![6jou](res/6jou.gif "6jou")

#### 6cpj
![6cpj](res/6cpj.gif "6cpj")

#### 1qg9
![1qg9](res/1qg9.gif "1qg9")
